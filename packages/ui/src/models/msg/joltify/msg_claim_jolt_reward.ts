import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

class MsgClaimJoltReward {
  public category: Categories;

  public type: string;

  public sender: string;

  // public amount: MsgCoin[];

  public json: object;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.sender = R.pathOr('', ['sender'], payload);
    // this.amount = R.pathOr<MsgClaimJoltReward['amount']>([], ['amount'], payload);
    this.json = R.pathOr({}, ['json'], payload);
  }

  static fromJson(json: object): MsgClaimJoltReward {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      sender: R.pathOr('', ['sender'], json),
      // amount: R.pathOr<MsgClaimJoltReward['amount']>([], ['amount'], json).map((x) => ({
      //   denom: x?.denom ?? '',
      //   amount: x?.amount ?? '0',
      // })),
    };
  }
}

export default MsgClaimJoltReward;
