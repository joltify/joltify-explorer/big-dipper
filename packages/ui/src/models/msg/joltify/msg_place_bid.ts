import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgPlaceBid {
  public category: Categories;

  public type: string;

  public amount: MsgCoin;

  public bidder: string;

  public auction_id: string;

  public json: object;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.bidder = R.pathOr('', ['bidder'], payload);
    this.auction_id = R.pathOr('', ['auction_id'], payload);
    this.amount = {
      denom: R.pathOr('', ['amount', 'denom'], ''),
      amount: R.pathOr('0', ['amount', 'amount'], '0'),
    };
    this.json = R.pathOr({}, ['json'], payload);
  }

  static fromJson(json: object): MsgPlaceBid {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      bidder: R.pathOr('', ['bidder'], json),
      auction_id: R.pathOr('', ['auction_id'], json),
      amount: {
        denom: R.pathOr('', ['amount', 'denom'], json),
        amount: R.pathOr('0', ['amount', 'amount'], json),
      },
    };
  }
}
