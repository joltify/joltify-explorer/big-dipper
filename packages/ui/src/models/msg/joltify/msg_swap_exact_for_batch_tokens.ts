import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSwapExactForBatchTokens {
  public category: Categories;

  public type: string;

  public json: object;

  public token_b: MsgCoin;

  public slippage: string;

  public requester: string;

  public exact_token_a: MsgCoin;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.token_b = {
      denom: R.pathOr('', ['token_b', 'denom'], ''),
      amount: R.pathOr('0', ['token_b', 'amount'], '0'),
    };
    this.exact_token_a = {
      denom: R.pathOr('', ['exact_token_a', 'denom'], ''),
      amount: R.pathOr('0', ['exact_token_a', 'amount'], '0'),
    };
    this.json = R.pathOr({}, ['json'], payload);
    this.slippage = R.pathOr('', ['slippage'], payload);
    this.requester = R.pathOr('', ['requester'], payload);
  }

  static fromJson(json: object): MsgSwapExactForBatchTokens {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      requester: R.pathOr('', ['requester'], json),
      token_b: {
        denom: R.pathOr('', ['token_b', 'denom'], json),
        amount: R.pathOr('0', ['token_b', 'amount'], json),
      },
      exact_token_a: {
        denom: R.pathOr('', ['exact_token_a', 'denom'], json),
        amount: R.pathOr('0', ['exact_token_a', 'amount'], json),
      },
      slippage: R.pathOr('', ['slippage'], json),
    };
  }
}
