import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSpvRepayInterest {
  public category: Categories;

  public type: string;

  public json: object;

  public creator: string;

  public pool_index: string;

  public repay_amount: MsgCoin;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.json = R.pathOr({}, ['json'], payload);
    this.creator = R.pathOr('', ['creator'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
    this.repay_amount = {
      denom: R.pathOr('', ['repay_amount', 'denom'], ''),
      amount: R.pathOr('0', ['repay_amount', 'amount'], '0'),
    };
  }

  static fromJson(json: object): MsgSpvRepayInterest {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      creator: R.pathOr('', ['creator'], json),
      pool_index: R.pathOr('', ['pool_index'], json),
      repay_amount: {
        denom: R.pathOr('', ['repay_amount', 'denom'], json),
        amount: R.pathOr('0', ['repay_amount', 'amount'], json),
      },
    };
  }
}
