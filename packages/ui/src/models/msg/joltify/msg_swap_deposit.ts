import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSwapDeposit {
  public category: Categories;

  public type: string;

  public json: object;

  public token_a: MsgCoin;

  public token_b: MsgCoin;

  public slippage: string;

  public depositor: string;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.token_b = {
      denom: R.pathOr('', ['token_b', 'denom'], ''),
      amount: R.pathOr('0', ['token_b', 'amount'], '0'),
    };
    this.token_a = {
      denom: R.pathOr('', ['token_a', 'denom'], ''),
      amount: R.pathOr('0', ['token_a', 'amount'], '0'),
    };
    this.json = R.pathOr({}, ['json'], payload);
    this.slippage = R.pathOr('', ['slippage'], payload);
    this.depositor = R.pathOr('', ['depositor'], payload);
  }

  static fromJson(json: object): MsgSwapDeposit {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      depositor: R.pathOr('', ['depositor'], json),
      token_b: {
        denom: R.pathOr('', ['token_b', 'denom'], json),
        amount: R.pathOr('0', ['token_b', 'amount'], json),
      },
      token_a: {
        denom: R.pathOr('', ['token_a', 'denom'], json),
        amount: R.pathOr('0', ['token_a', 'amount'], json),
      },
      slippage: R.pathOr('', ['slippage'], json),
    };
  }
}
