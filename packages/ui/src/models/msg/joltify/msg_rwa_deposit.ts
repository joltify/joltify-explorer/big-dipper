import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgRwaDeposit {
  public category: Categories;

  public type: string;

  public json: object;

  public token: MsgCoin;

  public creator: string;

  public pool_index: string;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.token = {
      denom: R.pathOr('', ['token', 'denom'], ''),
      amount: R.pathOr('0', ['token', 'amount'], '0'),
    };
    this.json = R.pathOr({}, ['json'], payload);
    this.creator = R.pathOr('', ['creator'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
  }

  static fromJson(json: object): MsgRwaDeposit {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      creator: R.pathOr('', ['creator'], json),
      token: {
        denom: R.pathOr('', ['token', 'denom'], json),
        amount: R.pathOr('0', ['token', 'amount'], json),
      },
      pool_index: R.pathOr('', ['pool_index'], json),
    };
  }
}
