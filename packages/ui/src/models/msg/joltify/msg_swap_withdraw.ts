import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSwapWithdraw {
  public category: Categories;

  public type: string;

  public json: object;

  public from: string;

  public token_a: MsgCoin;

  public token_b: MsgCoin;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.token_b = {
      denom: R.pathOr('', ['min_token_b', 'denom'], ''),
      amount: R.pathOr('0', ['min_token_b', 'amount'], '0'),
    };
    this.token_a = {
      denom: R.pathOr('', ['min_token_a', 'denom'], ''),
      amount: R.pathOr('0', ['min_token_a', 'amount'], '0'),
    };
    this.json = R.pathOr({}, ['json'], payload);
    this.from = R.pathOr('', ['from'], payload);
  }

  static fromJson(json: object): MsgSwapWithdraw {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      from: R.pathOr('', ['from'], json),
      token_b: {
        denom: R.pathOr('', ['min_token_b', 'denom'], json),
        amount: R.pathOr('0', ['min_token_b', 'amount'], json),
      },
      token_a: {
        denom: R.pathOr('', ['min_token_a', 'denom'], json),
        amount: R.pathOr('0', ['min_token_a', 'amount'], json),
      },
    };
  }
}
