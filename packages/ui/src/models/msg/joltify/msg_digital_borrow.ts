import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgDigitalBorrow {
  public category: Categories;

  public type: string;

  public borrower: string;

  public amount: MsgCoin[];

  public json: object;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.borrower = R.pathOr('', ['borrower'], payload);
    this.amount = R.pathOr<MsgDigitalBorrow['amount']>([], ['amount'], payload);
    this.json = R.pathOr({}, ['json'], payload);
  }

  static fromJson(json: object): MsgDigitalBorrow {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      borrower: R.pathOr('', ['borrower'], json),
      amount: R.pathOr<MsgDigitalBorrow['amount']>([], ['amount'], json).map((x) => ({
        denom: x?.denom ?? '',
        amount: x?.amount ?? '0',
      })),
    };
  }
}
