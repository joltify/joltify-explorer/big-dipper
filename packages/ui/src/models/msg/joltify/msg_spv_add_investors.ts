import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSpvAddInvestors {
  public category: Categories;

  public type: string;

  public json: object;

  public creator: string;

  public pool_index: string;

  public investor_iD: string[];

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.json = R.pathOr({}, ['json'], payload);
    this.creator = R.pathOr('', ['creator'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
    this.investor_iD = R.pathOr([], ['investor_iD'], payload);
  }

  static fromJson(json: object): MsgSpvAddInvestors {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      creator: R.pathOr('', ['creator'], json),
      pool_index: R.pathOr('', ['pool_index'], json),
      investor_iD: R.pathOr([], ['investor_iD'], json),
    };
  }
}
