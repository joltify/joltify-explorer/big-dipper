import * as R from 'ramda';
import type { Categories, Log } from '@/models/msg/types';
import chainConfig from '@/chainConfig';
import { formatToken } from '@/utils/format_token';

const { primaryTokenUnit } = chainConfig();

export default class MsgRwaClaimInterest {
  public category: Categories;

  public type: string;

  public json: object;

  public creator: string;

  public pool_index: string;

  public amounts: TokenUnit[];

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.json = R.pathOr({}, ['json'], payload);
    this.creator = R.pathOr('', ['creator'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
    this.amounts = R.pathOr([], ['amounts'], payload);
  }

  static getWithdrawalAmount(log?: Log) {
    console.log('log MsgClaimInterest', log?.events);
    const withdrawEvents = log?.events?.filter((x) => x.type === 'claim_interest') ?? [];
    const withdrawAmounts =
      withdrawEvents?.[0]?.attributes?.filter((x) => x.key === 'amount') ?? [];

    const amounts = (withdrawAmounts?.[0]?.value ?? '0').split(',').map((x) => {
      const [amount, denom = primaryTokenUnit] = x.match(/[a-z]+|[^a-z]+/gi) ?? [];
      return formatToken(amount, denom);
    });

    return amounts;
  }

  static fromJson(json: object, log?: Log): MsgRwaClaimInterest {
    const amounts = this.getWithdrawalAmount(log);
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      creator: R.pathOr('', ['creator'], json),
      pool_index: R.pathOr('', ['pool_index'], json),
      amounts,
    };
  }
}
