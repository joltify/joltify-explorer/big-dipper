import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgSpvUpdatePool {
  public category: Categories;

  public type: string;

  public json: object;

  public creator: string;

  public pool_apy: string;

  public pool_name: string;

  public pool_index: string;

  public target_token_amount: MsgCoin;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.json = R.pathOr({}, ['json'], payload);
    this.creator = R.pathOr('', ['creator'], payload);
    this.pool_apy = R.pathOr('', ['pool_apy'], payload);
    this.pool_name = R.pathOr('', ['pool_name'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
    this.target_token_amount = {
      denom: R.pathOr('', ['target_token_amount', 'denom'], ''),
      amount: R.pathOr('0', ['target_token_amount', 'amount'], '0'),
    };
  }

  static fromJson(json: object): MsgSpvUpdatePool {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      creator: R.pathOr('', ['creator'], json),
      pool_apy: R.pathOr('', ['pool_apy'], json),
      pool_name: R.pathOr('', ['pool_name'], json),
      pool_index: R.pathOr('', ['pool_index'], json),
      target_token_amount: {
        denom: R.pathOr('', ['target_token_amount', 'denom'], json),
        amount: R.pathOr('0', ['target_token_amount', 'amount'], json),
      },
    };
  }
}
