import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgDigitalWithdraw {
  public category: Categories;

  public type: string;

  public depositor: string;

  public amount: MsgCoin[];

  public json: object;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.depositor = R.pathOr('', ['depositor'], payload);
    this.amount = R.pathOr<MsgDigitalWithdraw['amount']>([], ['amount'], payload);
    this.json = R.pathOr({}, ['json'], payload);
  }

  static fromJson(json: object): MsgDigitalWithdraw {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      depositor: R.pathOr('', ['depositor'], json),
      amount: R.pathOr<MsgDigitalWithdraw['amount']>([], ['amount'], json).map((x) => ({
        denom: x?.denom ?? '',
        amount: x?.amount ?? '0',
      })),
    };
  }
}
