import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgDigitalRepay {
  public category: Categories;

  public type: string;

  public owner: string;

  public sender: string;

  public amount: MsgCoin[];

  public json: object;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.owner = R.pathOr('', ['owner'], payload);
    this.sender = R.pathOr('', ['sender'], payload);
    this.amount = R.pathOr<MsgDigitalRepay['amount']>([], ['amount'], payload);
    this.json = R.pathOr({}, ['json'], payload);
  }

  static fromJson(json: object): MsgDigitalRepay {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      owner: R.pathOr('', ['owner'], json),
      sender: R.pathOr('', ['sender'], json),
      amount: R.pathOr<MsgDigitalRepay['amount']>([], ['amount'], json).map((x) => ({
        denom: x?.denom ?? '',
        amount: x?.amount ?? '0',
      })),
    };
  }
}
