import * as R from 'ramda';
import type { Categories } from '@/models/msg/types';

export default class MsgClaimSPVReward {
  public category: Categories;

  public type: string;

  public json: object;

  public sender: string;

  public pool_index: string;

  constructor(payload: object) {
    this.category = 'joltify';
    this.type = R.pathOr('', ['type'], payload);
    this.json = R.pathOr({}, ['json'], payload);
    this.sender = R.pathOr('', ['sender'], payload);
    this.pool_index = R.pathOr('', ['pool_index'], payload);
  }

  static fromJson(json: object): MsgClaimSPVReward {
    return {
      category: 'joltify',
      json,
      type: R.pathOr('', ['@type'], json),
      sender: R.pathOr('', ['sender'], json),
      pool_index: R.pathOr('', ['pool_index'], json),
    };
  }
}
