import chainCoing from '@/chainConfig';
import useStyles from '@/components/ChainIcon/useStyles';
import Image, { type ImageProps } from 'next/image';

import baseIconLight from 'shared-utils/assets/icons/joltify-light.svg?url';
import baseLogoLight from 'shared-utils/assets/logos/joltify-light.svg?url';
import joltifyIconLight from 'shared-utils/assets/icons/joltify-light.svg?url';
import joltifyIconDark from 'shared-utils/assets/icons/joltify-dark.svg?url';
import joltifyLogoDark from 'shared-utils/assets/logos/joltify-dark.svg?url';
import joltifyLogoLight from 'shared-utils/assets/logos/joltify-light.svg?url';

interface IconProps extends Omit<ImageProps, 'id' | 'src'> {
  type: 'icon' | 'logo';
  chainName?: string;
}

const ChainIcon = ({
  className,
  type,
  chainName = chainCoing().chainName,
  ...props
}: IconProps) => {
  const { classes, cx } = useStyles();

  let [iconDark, iconLight] =
    type === 'icon' ? [baseIconLight, baseIconLight] : [baseLogoLight, baseLogoLight];
  switch (chainName) {
    case 'joltify':
      [iconDark, iconLight] =
        type === 'icon' ? [joltifyIconDark, joltifyIconLight] : [joltifyLogoDark, joltifyLogoLight];
      break;
    default:
      throw new Error(`chain ${chainName} not supported`);
  }
  return (
    <span className={cx(className, classes.container)}>
      <Image width={0} height={0} src={iconDark} {...props} className={classes.dark} unoptimized />
      <Image
        width={0}
        height={0}
        src={iconLight}
        {...props}
        className={classes.light}
        unoptimized
      />
    </span>
  );
};

export default ChainIcon;
