import Name from '@/components/name';
import { useProfileRecoil } from '@/recoil/profiles/hooks';
import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import useAppTranslation from '@/hooks/useAppTranslation';
import { FC } from 'react';
import { MsgDigitalDeposit } from '@/models';

const DigitalDeposit: FC<{ message: MsgDigitalDeposit }> = (props) => {
  const { t } = useAppTranslation('transactions');
  const { message } = props;

  const parsedAmount = message?.amount
    ?.map((x) => {
      const amount = formatToken(x.amount, x.denom);
      return `${formatNumber(amount.value, amount.exponent)} ${amount.displayDenom.toUpperCase()}`;
    })
    .reduce(
      (text, value, i, array) => text + (i < array.length - 1 ? ', ' : ` ${t('and')} `) + value
    );

  const depositor = useProfileRecoil(message.depositor);
  const recipientMoniker = depositor ? depositor?.name : message.depositor;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgDigitalDepositContent"
        components={[<Name address={message.depositor} name={recipientMoniker} />]}
        values={{
          amount: parsedAmount,
        }}
      />
    </Typography>
  );
};

export default DigitalDeposit;
