import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC, useState, useEffect } from 'react';
import { MsgClaimSPVReward } from '@/models';
import Name from '@/components/name';
import Big from 'big.js';

const ClaimSPVReward: FC<{ message: MsgClaimSPVReward }> = (props) => {
  const { message } = props;

  const [amount, setAmount] = useState<string>('');

  useEffect(() => {
    const txID = location.pathname.split('/joltify/transactions/').pop();
    if (!txID) return;
    const lcdMain = 'https://lcd.joltify.io';
    const lcdTestnet = 'https://rest-dev3.joltify.io';
    (async () => {
      let res = await (await fetch(`${lcdMain}/cosmos/tx/v1beta1/txs/${txID}`)).json();
      if (!res?.tx || !res?.tx_response) {
        res = await (await fetch(`${lcdTestnet}/cosmos/tx/v1beta1/txs/${txID}`)).json();
      }
      if (!res?.tx || !res?.tx_response) return;
      if (
        res.tx.body?.messages?.[0]?.['@type'] !==
        '/joltify.third_party.incentive.v1beta1.MsgClaimSPVReward'
      )
        return;
      const transferEvent = res.tx_response.logs?.[0]?.events?.find(
        (e: any) => e.type === 'transfer'
      );
      if (!transferEvent) return;
      const value = transferEvent?.attributes?.find((a: any) => a.key === 'amount')?.value;
      if (!value) return;
      const baseAmount = value.split('ujolt')[0] || '0';
      const amount = new Big(baseAmount).div(10 ** 6).toString();
      setAmount(amount);
    })();
  }, []);

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgClaimSPVRewardContent"
        components={[<Name address={message.sender} name={message.sender} />]}
        values={{
          pool_index: message.pool_index,
          amount,
        }}
      />
    </Typography>
  );
};

export default ClaimSPVReward;
