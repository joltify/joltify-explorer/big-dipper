import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSpvBorrow } from '@/models';
import Name from '@/components/name';

const SpvBorrow: FC<{ message: MsgSpvBorrow }> = (props) => {
  const { message } = props;

  const token = formatToken(message.borrow_amount?.amount, message.borrow_amount?.denom);
  const parsedToken = `${formatNumber(
    token.value,
    token.exponent
  )} ${token.displayDenom.toUpperCase()}`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSpvBorrowContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          token: parsedToken,
          pool_index: message.pool_index,
        }}
      />
    </Typography>
  );
};

export default SpvBorrow;
