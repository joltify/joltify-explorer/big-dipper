import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgRwaTransferOwnership } from '@/models';
import Name from '@/components/name';

const RwaTransferOwnership: FC<{ message: MsgRwaTransferOwnership }> = (props) => {
  const { message } = props;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgRwaTransferOwnershipContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          pool_index: message.pool_index,
        }}
      />
    </Typography>
  );
};

export default RwaTransferOwnership;
