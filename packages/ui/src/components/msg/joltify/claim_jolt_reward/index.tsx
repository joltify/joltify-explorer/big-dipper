import Name from '@/components/name';
import { MsgClaimJoltReward } from '@/models';
import { useProfileRecoil } from '@/recoil/profiles/hooks';
// import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
// import useAppTranslation from '@/hooks/useAppTranslation';
import { FC } from 'react';

const ClaimJoltReward: FC<{ message: MsgClaimJoltReward }> = (props) => {
  // const { t } = useAppTranslation('transactions');
  const { message } = props;

  // const parsedAmount = message?.amount?.map((x) => {
  //   const amount = formatToken(x.amount, x.denom);
  //   return `${formatNumber(amount.value, amount.exponent)} ${amount.displayDenom.toUpperCase()}`;
  // });
  // .reduce(
  //   (text, value, i, array) => text + (i < array.length - 1 ? ', ' : ` ${t('and')} `) + value
  // );

  window.console.log('message.recipient', message.sender);
  const recipient = useProfileRecoil(message.sender);
  window.console.log('recipient', recipient);
  const recipientMoniker = recipient ? recipient?.name : message.sender;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgClaimJoltRewardContent"
        components={[<Name address={message.sender} name={recipientMoniker} />]}
        // values={{
        //   amount: parsedAmount,
        // }}
      />
    </Typography>
  );
};

export default ClaimJoltReward;
