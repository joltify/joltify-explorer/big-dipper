import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgPlaceBid } from '@/models';
import Name from '@/components/name';

const PlaceBid: FC<{ message: MsgPlaceBid }> = (props) => {
  const { message } = props;

  const token = formatToken(message.amount?.amount, message.amount?.denom);
  const parsedToken = `${formatNumber(
    token.value,
    token.exponent
  )} ${token.displayDenom.toUpperCase()}`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgPlaceBidContent"
        components={[<Name address={message.bidder} name={message.bidder} />]}
        values={{
          amount: parsedToken,
          auction_id: message.auction_id,
        }}
      />
    </Typography>
  );
};

export default PlaceBid;
