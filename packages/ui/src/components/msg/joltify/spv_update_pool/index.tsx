import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSpvUpdatePool } from '@/models';
import Name from '@/components/name';
import Big from 'big.js';

const SpvUpdatePool: FC<{ message: MsgSpvUpdatePool }> = (props) => {
  const { message } = props;

  const token = formatToken(
    message.target_token_amount?.amount,
    message.target_token_amount?.denom
  );
  const parsedToken = `${formatNumber(
    token.value,
    token.exponent
  )} ${token.displayDenom.toUpperCase()}`;

  const pool_apy = `${new Big(message.pool_apy).times(100)}%`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSpvUpdatePoolContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          target_token_amount: parsedToken,
          pool_index: message.pool_index,
          pool_apy,
          pool_name: message.pool_name,
        }}
      />
    </Typography>
  );
};

export default SpvUpdatePool;
