import Name from '@/components/name';
import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSwapWithdraw } from '@/models';

const SwapWithdraw: FC<{ message: MsgSwapWithdraw }> = (props) => {
  const { message } = props;

  const token_a = formatToken(message.token_a?.amount, message.token_a?.denom);
  const token_a_parsed = `${formatNumber(
    token_a.value,
    token_a.exponent
  )} ${token_a.displayDenom.toUpperCase()}`;
  const token_b = formatToken(message.token_b?.amount, message.token_b?.denom);
  const token_b_parsed = `${formatNumber(
    token_b.value,
    token_b.exponent
  )} ${token_b.displayDenom.toUpperCase()}`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSwapWithdrawContent"
        components={[<Name address={message.from} name={message.from} />]}
        values={{
          token_a: token_a_parsed,
          token_b: token_b_parsed,
        }}
      />
    </Typography>
  );
};

export default SwapWithdraw;
