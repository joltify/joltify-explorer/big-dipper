import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSpvAddInvestors } from '@/models';
import Name from '@/components/name';

const SpvAddInvestors: FC<{ message: MsgSpvAddInvestors }> = (props) => {
  const { message } = props;

  const investor_iD = message.investor_iD.join(' ');

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSpvAddInvestorsContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          pool_index: message.pool_index,
          investor_iD,
        }}
      />
    </Typography>
  );
};

export default SpvAddInvestors;
