import Name from '@/components/name';
import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSwapExactForBatchTokens } from '@/models';
import Big from 'big.js';

const SwapExactForBatchTokens: FC<{ message: MsgSwapExactForBatchTokens }> = (props) => {
  const { message } = props;

  const exact_token_a = formatToken(message.exact_token_a?.amount, message.exact_token_a?.denom);
  const exact_token_a_parsed = `${formatNumber(
    exact_token_a.value,
    exact_token_a.exponent
  )} ${exact_token_a.displayDenom.toUpperCase()}`;
  const token_b = formatToken(message.token_b?.amount, message.token_b?.denom);
  const token_b_parsed = `${formatNumber(
    token_b.value,
    token_b.exponent
  )} ${token_b.displayDenom.toUpperCase()}`;

  const slippagePercent = `${new Big(message.slippage).div(10 ** 16)}%`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSwapExactForBatchTokensContent"
        components={[<Name address={message.requester} name={message.requester} />]}
        values={{
          exact_token_a: exact_token_a_parsed,
          token_b: token_b_parsed,
          slippage: slippagePercent,
        }}
      />
    </Typography>
  );
};

export default SwapExactForBatchTokens;
