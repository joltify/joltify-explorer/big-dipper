import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC } from 'react';
import { MsgSpvRepayInterest } from '@/models';
import Name from '@/components/name';

const SpvRepayInterest: FC<{ message: MsgSpvRepayInterest }> = (props) => {
  const { message } = props;

  console.log('message', message);

  const token = formatToken(message.json?.token?.amount, message.json?.token?.denom);
  const parsedToken = `${formatNumber(
    token.value,
    token.exponent
  )} ${token.displayDenom.toUpperCase()}`;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgSpvRepayInterestContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          token: parsedToken,
          pool_index: message.pool_index,
        }}
      />
    </Typography>
  );
};

export default SpvRepayInterest;
