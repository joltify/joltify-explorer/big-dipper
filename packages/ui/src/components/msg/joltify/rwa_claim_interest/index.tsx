import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import { FC, useState, useEffect } from 'react';
import { MsgRwaClaimInterest } from '@/models';
import Name from '@/components/name';
import chain from '../../../../../../../apps/web-joltify/src/chain.json';
import Big from 'big.js';

const tokenUnits = chain.chains[0].tokenUnits;
type TokenUnitKeys = keyof typeof tokenUnits;

const RwaClaimInterest: FC<{ message: MsgRwaClaimInterest }> = (props) => {
  const { message } = props;

  const [amount, setAmount] = useState<string>('');

  useEffect(() => {
    const txID = location.pathname.split('/joltify/transactions/').pop();
    if (!txID) return;
    const lcdMain = 'https://lcd.joltify.io';
    const lcdTestnet = 'https://rest-dev3.joltify.io';
    (async () => {
      let res = await (await fetch(`${lcdMain}/cosmos/tx/v1beta1/txs/${txID}`)).json();
      if (!res?.tx || !res?.tx_response) {
        res = await (await fetch(`${lcdTestnet}/cosmos/tx/v1beta1/txs/${txID}`)).json();
      }
      if (!res?.tx || !res?.tx_response) return;
      if (res.tx.body?.messages?.[0]?.['@type'] !== '/joltify.spv.MsgClaimInterest') return;
      const transferEvent = res.tx_response.logs?.[0]?.events?.find(
        (e: any) => e.type === 'transfer'
      );
      if (!transferEvent) return;
      const value = transferEvent?.attributes?.find((a: any) => a.key === 'amount')?.value;
      if (!value) return;
      const { amount, denom } = coinStringConverter(value);
      let tokenUnit = tokenUnits[!denom as unknown as TokenUnitKeys];
      for (const key in tokenUnits) {
        if (key === denom) {
          tokenUnit = tokenUnits[key as TokenUnitKeys];
        }
      }
      const _amount = new Big(amount).div(10 ** tokenUnit.exponent).toString();
      const hrAmountString = `${_amount} ${tokenUnit.display.toUpperCase()}`;
      setAmount(hrAmountString);
    })();
  }, []);

  function coinStringConverter(amountString: string) {
    const match = amountString?.match(/^(\d+)(.*)$/);
    const amount = match?.[1];
    const denom = match?.[2];
    return { amount, denom };
  }

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgRwaClaimInterestContent"
        components={[<Name address={message.creator} name={message.creator} />]}
        values={{
          pool_index: message.pool_index,
          amount,
        }}
      />
    </Typography>
  );
};

export default RwaClaimInterest;
