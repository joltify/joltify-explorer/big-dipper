import Name from '@/components/name';
import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import useAppTranslation from '@/hooks/useAppTranslation';
import { FC } from 'react';
import { MsgDigitalRepay } from '@/models';

const DigitalRepay: FC<{ message: MsgDigitalRepay }> = (props) => {
  const { t } = useAppTranslation('transactions');
  const { message } = props;

  const parsedAmount = message?.amount
    ?.map((x) => {
      const amount = formatToken(x.amount, x.denom);
      return `${formatNumber(amount.value, amount.exponent)} ${amount.displayDenom.toUpperCase()}`;
    })
    .reduce(
      (text, value, i, array) => text + (i < array.length - 1 ? ', ' : ` ${t('and')} `) + value
    );

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgDigitalRepayContent"
        components={[
          <Name address={message.sender} name={message.sender} />,
          <b />,
          <Name address={message.owner} name={message.owner} />,
        ]}
        values={{
          amount: parsedAmount,
        }}
      />
    </Typography>
  );
};

export default DigitalRepay;
