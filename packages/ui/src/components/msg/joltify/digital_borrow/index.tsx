import Name from '@/components/name';
import { useProfileRecoil } from '@/recoil/profiles/hooks';
import { formatNumber, formatToken } from '@/utils/format_token';
import Typography from '@mui/material/Typography';
import AppTrans from '@/components/AppTrans';
import useAppTranslation from '@/hooks/useAppTranslation';
import { FC } from 'react';
import { MsgDigitalBorrow } from '@/models';

const DigitalBorrow: FC<{ message: MsgDigitalBorrow }> = (props) => {
  const { t } = useAppTranslation('transactions');
  const { message } = props;

  const parsedAmount = message?.amount
    ?.map((x) => {
      const amount = formatToken(x.amount, x.denom);
      return `${formatNumber(amount.value, amount.exponent)} ${amount.displayDenom.toUpperCase()}`;
    })
    .reduce(
      (text, value, i, array) => text + (i < array.length - 1 ? ', ' : ` ${t('and')} `) + value
    );

  const depositor = useProfileRecoil(message.borrower);
  const recipientMoniker = depositor ? depositor?.name : message.borrower;

  return (
    <Typography>
      <AppTrans
        i18nKey="message_contents:MsgDigitalBorrowContent"
        components={[<Name address={message.borrower} name={recipientMoniker} />]}
        values={{
          amount: parsedAmount,
        }}
      />
    </Typography>
  );
};

export default DigitalBorrow;
